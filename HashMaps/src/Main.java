import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        // MyHashMap<Integer, Person> map = new MyHashMap<>();
        // Person p1 = new Person("Bob", 4362);
        // Person p2 = new Person("Sam", 9530);
        // Person p3 = new Person("Sally", 7930);
        // System.out.println("Print empty map:");
        // map.printMap();// Should print empty map of size 31.
        // System.out.println();
        //
        // // Try inserting elements with obvious hash indices.
        // map.put(1, p1);// Should be inserted in bucket 1.
        // map.put(2, p2);
        // map.put(3, p3);
        // System.out.println("Print map with 3 items:");
        // map.printMap();// Should print map with 3 items.
        // System.out.println();
        //
        // Person p4 = new Person("Samuel", 2952);
        // // 64%2 = 1. 1%3 = 1. Hence, we are forcing a collision without
        // // resizing.
        // map.put(64, p4);
        // System.out.println("Print map with 4 items, two in one bucket:");
        // map.printMap();// Should be 2 Persons in bucket 2.
        // System.out.println();
        //
        // Person p5 = new Person("Samantha", 6920);
        // map.put(95, p5);// Force one more collision in bucket 2.
        // System.out.println("Print map with 5 items, map resized:");
        // // Everything should be rearranged, no more than 2 collisions.
        // map.printMap();
        // // Number of buckets should also be 67, because the next prime up
        // from
        // // 31*2 is 67.
        // System.out.println();
        // System.out.println("Map contains key 95: " + map.containsKey(95));//
        // True
        // System.out
        // .println("Map contains value of p3: " + map.containsValue(p3));//
        // True
        // System.out.println("Map contains key 291: " +
        // map.containsKey(291));// False
        //
        // System.out.println();
        // Person retrieved = map.get(95);
        // System.out.println("Person at key 95: " + retrieved);
        // map.remove(95);// Removes the person found above from the map.
        // map.printMap();// List should only have 4 elements now.
        //
        // map.clear();
        // System.out.println("Map cleared.");
        // System.out.println("Map is empty? " + map.isEmpty());// Should be
        // true.
        // map.printMap();
        // ---------------------------------------------------------------------
        System.out.println("Now testing HashSet.");
        // ---------------------------------------------------------------------
        MyHashSet<Person> set = new MyHashSet<>();
        Person per1 = new Person("Adam Jensen", 4629);
        Person per2 = new Person("Adam Jensen", 4221);
        Person per3 = new Person("Adam Jackson", 4629);
        set.add(per1);
        set.add(per2);
        set.add(per3);
        // Should only hold 2 items. Adam Jackson should not be added.
        set.printSet();
        System.out.println("Set contains Per2: " + set.containsValue(per2));
        set.remove(per2);
        set.printSet();// Should contain 1 item.
        set.clear();
        System.out.println("Set is empty?: " + set.isEmpty());// True
    }
}















class Person {

    private String name;
    private int id;



    public Person(String s, int id) {
        name = s;
        this.id = id;
    }



    /**
     * We want every Person to be unique according to their ID number. If two
     * person objects are the same, they must have the same ID number.
     */
    public boolean equals(Object arg0) {
        if (!(arg0 instanceof Person))
            return false;
        Person other = (Person) arg0;
        return this.id == other.id;
    }



    /**
     * Since we want every Personto be unique by their id number, we might as
     * well return the id number as is. If two Person objects are the same,
     * their hashCodes should return the same number.
     */
    @Override
    public int hashCode() {
        return id;
    }



    @Override
    public String toString() {
        return name + " (" + id + ")";
    }
}















/**
 * A HashMap made from scratch. The underlying data structure will be an
 * ArrayList of LinkedLists.
 *
 * @author Toni-Tran
 * @param <K>
 *            The type of keys that will be used to map items into the HashMap.
 * @param <V>
 *            The values that will be stored in the HashMap.
 */
class MyHashMap<K, V> {

    private ArrayList<LinkedList<HashMapEntry>> mList;
    private int mNumBuckets = 31;
    private int mBucketSizeLimit = 2;



    /**
     * Default constructor for the HashMap.
     */
    public MyHashMap() {
        mList = new ArrayList<>();
        for (int i = 0; i < mNumBuckets; i++) {
            mList.add(new LinkedList<HashMapEntry>());
        }
    }



    /**
     * Returns the number of entries currently stored in the HashMap.
     *
     * @return the number of entries currently stored in the HashMap.
     */
    public int size() {
        int currentSize = 0;
        for (LinkedList<HashMapEntry> bucket : mList) {
            for (HashMapEntry entry : bucket) {
                currentSize++;
            }
        }
        return currentSize;
    }



    /**
     * @return true if the HashMap has 0 entries.
     */
    public boolean isEmpty() {
        return this.size() == 0;
    }



    /**
     * @param key
     *            The key to search for.
     * @return True if the key exists in the HashMap, or false otherwise.
     */
    public boolean containsKey(K key) {
        int hashedIndex = key.hashCode() % mNumBuckets;
        LinkedList<HashMapEntry> bucket = mList.get(hashedIndex);
        for (HashMapEntry e : bucket) {
            if (e.key.equals(key)) {
                return true;
            }
        }
        return false;
    }



    /**
     * @param value
     *            The value to search for.
     * @return True if the value exists in the HashMap, or false otherwise.
     */
    public boolean containsValue(V value) {
        for (LinkedList<HashMapEntry> bucket : mList) {
            for (HashMapEntry entry : bucket) {
                if (entry.value.equals(value)) {
                    return true;
                }
            }
        }
        return false;
    }



    /**
     * Returns the element found at the hashed index bucket with the same key.
     *
     * @param key
     *            The key to search for.
     * @return The value stored at the location, or null if it is not found.
     */
    public V get(K key) {
        int hashedIndex = key.hashCode() % mNumBuckets;
        LinkedList<HashMapEntry> bucket = mList.get(hashedIndex);
        for (HashMapEntry e : bucket) {
            if (e.key.equals(key)) {
                return e.value;
            }
        }
        return null;
    }



    /**
     * Puts an item into the HashMap based on a key and value. If the key
     * already exists in the map, this method replaces the item and returns the
     * old item that was previously there. If not, it returns null.
     *
     * @param key
     *            The unique key to use to find a location to insert in the
     *            HashMap.
     *
     * @param value
     *            The value to store in the HashMap. Note that duplicate values
     *            may exist, as well as null ones. It is the keys that must be
     *            unique.
     * @return The old value that was there, if a replacement occurred, or null
     *         if not.
     */
    public V put(K key, V value) {
        int hashedIndex = key.hashCode() % mNumBuckets;
        LinkedList<HashMapEntry> bucket = mList.get(hashedIndex);
        // If the bucket is empty, simply add the entry.
        if (bucket.size() == 0) {
            bucket.addLast(new HashMapEntry(key, value));
        }
        // Else, search the bucket for existing entries.
        for (int i = 0; i < bucket.size(); i++) {
            // An item with the same key in the bucket exists. Replace the
            // value.
            if (bucket.get(i).key.equals(key)) {
                V oldItem = bucket.get(i).value;
                bucket.set(i, new HashMapEntry(key, value));
                return oldItem;
            }
            // Else, a duplicate key was not found. Check to see if the bucket
            // is at its size limit.
            int bucketSize = bucket.size();
            if (bucketSize == mBucketSizeLimit) {
                // The bucket has reached its limit. Increase the size and
                // re-insert the item.
                this.increaseHashMapSize();
                return put(key, value);
            }
            bucket.addLast(new HashMapEntry(key, value));
            return null;
        }
        return null;
    }



    /**
     * Removes an object in the map with the particular key.
     *
     * @param key
     *            The key to use to find a match.
     * @return The object that was removed, or null if it was not found.
     */
    public V remove(K key) {
        int hashedIndex = key.hashCode() % mNumBuckets;
        LinkedList<HashMapEntry> bucket = mList.get(hashedIndex);
        for (HashMapEntry e : bucket) {
            if (e.key.equals(key)) {
                V toReturn = e.value;
                bucket.remove(e);
                return toReturn;
            }
        }
        return null;
    }



    /**
     * Clears the entire HashMap. The number of buckets should remain the same.
     */
    public void clear() {
        // mList.clear();
        for (LinkedList<HashMapEntry> bucket : mList) {
            bucket.clear();
        }
    }



    /**
     * Increases the size of the HashMap, reinserting all the elements that were
     * previously there.
     */
    private void increaseHashMapSize() {
        int oldSize = this.size();// Get the old size of elements currently in
                                  // the HashMap.
        ArrayList<HashMapEntry> previousEntries = new ArrayList<HashMapEntry>(
                oldSize);
        for (LinkedList<HashMapEntry> bucket : mList) {
            for (HashMapEntry e : bucket) {
                previousEntries.add(e);
            }
        }
        // Find the next prime number closest to the current amount of buckets,
        // doubled.
        mNumBuckets = nextPrimeAfterDoubled(mNumBuckets);
        // Clears the old map, reinstantiates with the new size.
        this.clear();
        int oldNumberOfBuckets = mList.size();
        for (int i = 0; i < mNumBuckets - oldNumberOfBuckets; i++) {
            mList.add(new LinkedList<>());
        }
        // Reinsert all the old values.
        HashMapEntry entry;
        for (int i = 0; i < oldSize; i++) {
            entry = previousEntries.get(i);
            this.put(entry.key, entry.value);
        }
    }



    /**
     * Prints the entire map for debugging.
     */
    public void printMap() {
        System.out.println("Number of buckets: " + mNumBuckets);
        System.out.println("Number of items allowed per bucket: "
                + mBucketSizeLimit);
        System.out.println("Size of map: " + this.size());
        int index = 0;
        for (LinkedList<HashMapEntry> bucket : mList) {
            System.out.print("Index " + index + " > ");
            for (HashMapEntry entry : bucket) {
                System.out.print("[Key: " + entry.key + ", Value: "
                        + entry.value + "], ");
            }
            System.out.println("");
            index++;
        }
    }



    /**
     * Returns an ArrayList of all the keys held within the HashMap.
     * 
     * @return
     */
    public ArrayList<K> keyList() {
        ArrayList<K> keyList = new ArrayList<>();
        for (LinkedList<HashMapEntry> bucket : mList) {
            for (HashMapEntry entry : bucket) {
                keyList.add(entry.key);
            }// TODO
        }
        return keyList;
    }



    /**
     * Find the next prime number closest to the current amount of buckets,
     * doubled. For example, if the current prime is 7, this method will double
     * it (14), and then find the next prime number closest, upwards. (17).
     *
     * @return the next prime number closest to double the current amount of
     *         buckets.
     */
    private int nextPrimeAfterDoubled(int current) {
        int doubled = current * 2;
        BigInteger bigInt = new BigInteger(Integer.toString(doubled));
        bigInt = bigInt.nextProbablePrime();
        return Integer.parseInt(bigInt.toString());
    }

    /**
     * A wrapper class that acts as the [Key,Value] pair that will be stored in
     * each bucket.
     */
    class HashMapEntry {

        K key;
        V value;



        public HashMapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
}















/**
 * A HashSet is simply a wrapper around a HashMap. As such, all methods will be
 * utilize the already existent HashMap.
 * 
 * @author Toni-Tran
 *
 * @param <V>
 *            The type of objects to be stored in the HashSet.
 */
class MyHashSet<V> {

    // The underlying data type of a HashSet. Rather than storing items as
    // <Key,Value> pairs, we store only the <Value>, using the object's
    // hashCode() itself as the key.
    MyHashMap<Integer, V> map;



    /**
     * Default constructor.
     */
    public MyHashSet() {
        map = new MyHashMap<>();
    }



    /**
     * Adds an item into the HashSet.
     * 
     * @param value
     *            The item to add.
     */
    public boolean add(V value) {
        if (map.containsValue(value))
            return false;
        map.put(value.hashCode(), value);
        return true;
    }



    /**
     * Returns the number of entries currently stored in the HashSet.
     *
     * @return the number of entries currently stored in the HashSet.
     */
    public int size() {
        return map.size();
    }



    /**
     * @return true if the HashSet has 0 entries.
     */
    public boolean isEmpty() {
        return map.size() == 0;
    }



    /**
     * @param value
     *            The value to search for.
     * @return True if the value exists in the HashSet, or false otherwise.
     */
    public boolean containsValue(V value) {
        return map.containsValue(value);
    }



    /**
     * Removes an item from the HashSet.
     * 
     * @param value
     *            The value to remove.
     * @return True if the item was removed, false if it was never there.
     */
    public boolean remove(V value) {
        V removed = map.remove(value.hashCode());
        return removed == null;
    }



    /**
     * Clears the entire HashSet.
     */
    public void clear() {
        map.clear();
    }



    /**
     * Prints the entire set for debugging.
     */
    public void printSet() {
        System.out.println("Size: " + size());
        ArrayList<Integer> keyList = map.keyList();
        V value;
        for (int i : keyList) {
            value = map.get(i);
            System.out.println(value + ", ");
        }
    }

}
